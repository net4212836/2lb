var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

List<Company> companyList = new List<Company>();
builder.Configuration.AddJsonFile("google.json");
Company google = new Company();
app.Configuration.Bind(google);
builder.Configuration.AddIniFile("microsoft.ini");
Company microsoft = new Company();
app.Configuration.Bind(microsoft);
builder.Configuration.AddXmlFile("apple.xml");
Company apple = new Company();
app.Configuration.Bind(apple);
companyList.Add(google);
companyList.Add(microsoft);
companyList.Add(apple);
app.UseMiddleware<CompanyMiddleware>(companyList);
builder.Configuration.AddJsonFile("student.json");
Student student = new Student();
app.Configuration.Bind(student);
app.MapGet("/", (context) => context.Response.WriteAsync( student.ToString()));

app.Run();
