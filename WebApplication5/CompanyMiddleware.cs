﻿public class CompanyMiddleware
{
    readonly RequestDelegate next;
    readonly List<Company> companies;

    public CompanyMiddleware(RequestDelegate next, List<Company> companies)
    {
        this.next = next;
        this.companies = companies;
    }
    public async Task InvokeAsync(HttpContext context)
    {
        await next.Invoke(context);
        string companyNameWithMaxEmployees = FindCompanyWithMaxEmployees(companies);
        await context.Response.WriteAsync(companyNameWithMaxEmployees);

    }

    private string FindCompanyWithMaxEmployees(List<Company> companies)
    {
        int maxEmployees = companies.Max(c => c.NumWorkers);
        var companyWithMaxEmployees = companies.Find(c => c.NumWorkers == maxEmployees);
        return companyWithMaxEmployees == null ? "\nNo company found with the maximum number of employees" : $"\nCompany name: {companyWithMaxEmployees.Name}, number of workers: {companyWithMaxEmployees.NumWorkers}";
    }
}
