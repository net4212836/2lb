﻿   public class Student
    {
    public string Name { get; set; } = "";
    public int Age { get; set; }
    public string Location { get; set; } = "";
    public int ExperienceYears { get; set; }

    public override string ToString()
    {
        return "Name: "+ Name + ", Age: " + Age + ", Location: " + Location + ", Years of experience: "+ ExperienceYears;
    }
}

